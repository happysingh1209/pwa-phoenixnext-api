import { apiStatus } from '../../../lib/util';
import { Router } from 'express';

const Magento2Client = require('magento2-rest-client').Magento2Client;

module.exports = ({ config, db }) => {
  let mcApi = Router();

  mcApi.post('/contact', (req, res) => {
    let contactData = req.body
    if (!contactData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('contact', (restClient) => {
      var module = {};
      module.contact = function () {
        return restClient.post('/contact', contactData)
      }
      return module;
    })

    client.contact.contact().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/socialSignup', (req, res) => {
    let socialData = req.body
    if (!socialData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('socialSignup', (restClient) => {
      var module = {};
      module.socialSignup = function () {
        return restClient.put('/aureatelabs/socialSignup', socialData)
      }
      return module;
    })

    client.socialSignup.socialSignup().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getrewards', (req, res) => {
    let customerId = req.body.u_id
    if (!customerId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('rewards', (restClient) => {
      var module = {};
      module.rewards = function () {
        return restClient.get('/getrewardshistory?customer_id=' + customerId)
      }
      return module;
    })

    client.rewards.rewards().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getproductrewards', (req, res) => {
    let productId = req.body.product_id
    if (!productId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('rewards', (restClient) => {
      var module = {};
      module.rewards = function () {
        return restClient.get('/aureatelabs-productrewardpoint/getpoints?product_id=' + productId)
      }
      return module;
    })

    client.rewards.rewards().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getfreegiftproducts', (req, res) => {
    let quoteId = req.body.quote_id
    if (!quoteId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('giftproducts', (restClient) => {
      var module = {};
      module.giftproducts = function () {
        return restClient.get('/aureatelabs-freegift/getdata?quote_id=' + quoteId)
      }
      return module;
    })

    client.giftproducts.giftproducts().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/addfreegift', (req, res) => {
    console.log("comming on this event")

    let quote = req.body.quote
    let rule  = req.body.rule
    let gift  = req.body.gift

    if (!quote && !rule && !gift) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('addgiftproducts', (restClient) => {
      var module = {};
      module.addgiftproducts = function () {
        return restClient.post('/mpfreegifts/gift/add/quote/' + quote + '/rule/' + rule + "/gift/" + gift)
      }
      return module;
    })

    client.addgiftproducts.addgiftproducts().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  return mcApi
}
